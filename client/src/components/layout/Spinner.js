import React, { Fragment } from 'react';

import infinityLoader from 'img/infinityLoader.gif';

export default () => (
	<Fragment>
		<img
			src={infinityLoader}
			style={{
				borderRadius: '100%',
				opacity: '0.8'
			}}
			alt='Loading...'
		/>
	</Fragment>
);
